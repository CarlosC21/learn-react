from django.urls import path 
from .views import get_one_appointment, get_technician, make_appointment

urlpatterns = [
    path(
        "technicians/", get_technician, name="get_technician",
    ),
    path("service/", make_appointment, name="make_appointment"),
    path("service/<int:pk>/", get_one_appointment, name="get_one_appointment"),
    # path("vins/", get_vins, name="get_vins")
]
