import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <h3>Service</h3>
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <NavLink className="navbar-brand" to="/services/technician/enter">
              <li>New Technician</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/services/appointment/enter">
              <li>New Appointment</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/services/appointments/list">
              <li>Appointment List</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/services/appointments/history">
              <li>Appointment History</li>
            </NavLink>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
